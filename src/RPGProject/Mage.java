package RPGProject;

import RPGProject.*;

public class Mage extends Hero {



    /*private PrimaryAttribut basePrimayAttribut = new PrimaryAttribut();
    private PrimaryAttribut totalPriamryAttribut = new PrimaryAttribut();
    private SecondaryAttribut secondaryAttribut = new SecondaryAttribut();
    private HashMap<Slot, Item> equipment = new HashMap<>();*/


    public Mage (String name, int level) {
        super(name, level, 1, 5, 8, 1);
        /*this.basePrimayAttribut.setStrength(5);
        this.basePrimayAttribut.setVitality(10);
        this.basePrimayAttribut.setDexterity(2);
        this.basePrimayAttribut.setIntelligence(1);
        equipment.put(Slot.WEAPON,new Item());
        equipment.put(Slot.HEAD,new Item());
        equipment.put(Slot.BODY,new Item());
        equipment.put(Slot.LEGS,new Item());
        setTotalPriamryAttributTwo();
        setSecondaryStats();*/

    }



  /*   public PrimaryAttribut getTotalPriamryAttribut() {
        return totalPriamryAttribut;
    }


   public PrimaryAttribut getBasePrimayAttribut() {
        return basePrimayAttribut;
    }

    public void setBasePrimayAttribut() {
        this.basePrimayAttribut.setStrength(5);
        this.basePrimayAttribut.setVitality(10);
        this.basePrimayAttribut.setDexterity(2);
        this.basePrimayAttribut.setIntelligence(1);
    }*/

   /* public void setTotalPriamryAttribut(PrimaryAttribut basePriamryAttribut, Item head, Item body, Item legs) {
        this.totalPriamryAttribut.setStrength(basePriamryAttribut.getStrength() + head.getStrength() + body.getStrength() + legs.getStrength());
        this.totalPriamryAttribut.setDexterity(basePriamryAttribut.getDexterity() + head.getDexterity() + body.getDexterity() + legs.getDexterity());
        this.totalPriamryAttribut.setIntelligence(basePriamryAttribut.getIntelligence() + head.getIntelligence() + body.getIntelligence() + legs.getIntelligence());
        this.totalPriamryAttribut.setVitality(basePriamryAttribut.getVitality() + head.getVitality() + body.getVitality() + legs.getVitality());
    }
    public void setTotalPriamryAttributTwo(){


            this.totalPriamryAttribut.setStrength(basePrimayAttribut.getStrength() + getEquipment(Slot.HEAD).getStrength() + getEquipment(Slot.BODY).getStrength() + getEquipment(Slot.LEGS).getStrength());
            this.totalPriamryAttribut.setDexterity(basePrimayAttribut.getDexterity() + getEquipment(Slot.HEAD).getDexterity() + getEquipment(Slot.BODY).getDexterity() + getEquipment(Slot.LEGS).getDexterity());
            this.totalPriamryAttribut.setIntelligence(basePrimayAttribut.getIntelligence() + getEquipment(Slot.HEAD).getIntelligence() + getEquipment(Slot.BODY).getIntelligence() + getEquipment(Slot.LEGS).getIntelligence());
            this.totalPriamryAttribut.setVitality(basePrimayAttribut.getVitality() +getEquipment(Slot.HEAD).getVitality() +getEquipment(Slot.BODY).getVitality() + getEquipment(Slot.LEGS).getVitality());

        }

    public void setSecondaryStats(){
        this.secondaryAttribut.setHealth(this.totalPriamryAttribut.getVitality());
        this.secondaryAttribut.setArmorRating(this.totalPriamryAttribut.getStrength(), this.totalPriamryAttribut.getDexterity());
        this.secondaryAttribut.setElementalResistance(this.totalPriamryAttribut.getIntelligence());
    }

    public SecondaryAttribut getSecondaryAttribut() {
        return secondaryAttribut;
    }*/


    @Override
    public boolean equipArmor(Slot slot, Item item) {


        boolean wrongArmorType=true;
        boolean lvlToHigh=true;

        try {

            if (item.getArmor() != Armor.NONE
                    && item.getArmor() == Armor.CLOTH
                    && item.getSlot() == slot) {
                wrongArmorType=false;
                if (this.getLevel() >= item.getLvlReg()) {
                    getAllEquipment().put(slot, item);
                    lvlToHigh=false;
                }

            } if (wrongArmorType){
                throw new RBGExceptions("Wrong Armor type");
            }else if (!wrongArmorType){
                if (lvlToHigh){
                    throw  new RBGExceptions("Armor has to high lvl");
                }
            }
        }catch (RBGExceptions ex){
            System.out.println(ex.getMessage());
        }

        setTotalPriamryAttributTwo();
        setSecondaryStats();
        if (wrongArmorType || lvlToHigh){
            return false;
        }else {
            return true;
        }

    }


    @Override
    public boolean equipWeapon(Slot slot, Item item) throws RBGExceptions {

        boolean wrongWeaponType=true;
        boolean lvlToHigh=true;
            if (item.getWeapons() != Weapons.NONE
                    && (item.getWeapons() == Weapons.STAFFS || item.getWeapons() == Weapons.WANDS)
                    && item.getSlot() == slot) {
                wrongWeaponType=false;
                if (this.getLevel() >= item.getLvlReg()) {
                    getAllEquipment().put(slot, item);
                    lvlToHigh=false;
                }
            }
            if (wrongWeaponType){
                throw new RBGExceptions("Wrong weapon type");
            }else if (!wrongWeaponType){
                if (lvlToHigh){
                    throw  new RBGExceptions("Weapon has to high lvl");
                }
            }

        setTotalPriamryAttributTwo();
        setSecondaryStats();
        if (wrongWeaponType || lvlToHigh){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void levelUp() {
        setBasePrimayAttribut(getBasePrimayAttribut().getStrength() + 1, getBasePrimayAttribut().getVitality() + 3, getBasePrimayAttribut().getIntelligence() + 5, getBasePrimayAttribut().getDexterity() + 1);
        /*this.basePrimayAttribut.setStrength(this.basePrimayAttribut.getStrength()+3);
        this.basePrimayAttribut.setVitality(this.basePrimayAttribut.getVitality()+5);
        this.basePrimayAttribut.setIntelligence(this.basePrimayAttribut.getIntelligence()+1);
        this.basePrimayAttribut.setDexterity(this.basePrimayAttribut.getDexterity()+2);*/
        setTotalPriamryAttributTwo();
        setSecondaryStats();
        setLevel(getLevel() + 1);
    }

    @Override
    public Item getEquipment(Slot slot) {
        return getAllEquipment().get(slot);
    }

    @Override
    public void setHeroDPS(Item weapon) {
        heroDPS = weapon.getWeaponDps() * (1 + (getTotalPriamryAttribut().getStrength() / 100));
    }

    @Override
    public double getHeroDPS() {
        return heroDPS;
    }
}
