package RPGProject;

import java.util.HashMap;

public abstract class Hero implements Upgradeable {

    private String name;
    private double level;
    private PrimaryAttribut basePrimayAttribut = new PrimaryAttribut();
    private PrimaryAttribut totalPriamryAttribut = new PrimaryAttribut();
    private SecondaryAttribut secondaryAttribut = new SecondaryAttribut();
    private HashMap<Slot, Item> equipment = new HashMap<>();
    protected double heroDPS=1.0;


    public Hero() {

    }


    public Hero(String name, double level) {
        this.name = name;
        this.level = level;
    }

    public Hero(String name, double level, double strength, double vitality, double intellect, double dexterity) {
        this.name = name;
        this.level = level;
        this.basePrimayAttribut.setStrength(strength);
        this.basePrimayAttribut.setDexterity(dexterity);
        this.basePrimayAttribut.setIntelligence(intellect);
        this.basePrimayAttribut.setVitality(vitality);
        equipment.put(Slot.WEAPON, new Item());
        equipment.put(Slot.HEAD, new Item());
        equipment.put(Slot.BODY, new Item());
        equipment.put(Slot.LEGS, new Item());
        setTotalPriamryAttributTwo();
        setSecondaryStats();
    }

    public HashMap<Slot, Item> getAllEquipment() {
        return equipment;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLevel() {
        return level;
    }

    public void setLevel(double level) {

        if (getLevel() >= level) {

            throw new IllegalArgumentException(" The hero is already level " + getLevel() + " it cannot gain level " + level);
        }else if (level<=0){
            throw new IllegalArgumentException(" The hero is already level " + getLevel() + " it cannot gain level " + level);
        }
        this.level = level;
    }

    public void setBasePrimayAttribut(double strength, double vitality, double intellect, double dexterity) {
        basePrimayAttribut.setStrength(strength);
        basePrimayAttribut.setVitality(vitality);
        basePrimayAttribut.setIntelligence(intellect);
        basePrimayAttribut.setDexterity(dexterity);
        //setTotalPriamryAttributTwo();
    }


    public PrimaryAttribut getBasePrimayAttribut() {
        return basePrimayAttribut;
    }

    public PrimaryAttribut getTotalPriamryAttribut() {
        return totalPriamryAttribut;
    }

    public SecondaryAttribut getSecondaryAttribut() {
        return secondaryAttribut;
    }


    public void setTotalPriamryAttributTwo() {

        this.totalPriamryAttribut.setStrength(basePrimayAttribut.getStrength() + getEquipment(Slot.HEAD).getStrength() + getEquipment(Slot.BODY).getStrength() + getEquipment(Slot.LEGS).getStrength());
        this.totalPriamryAttribut.setDexterity(basePrimayAttribut.getDexterity() + getEquipment(Slot.HEAD).getDexterity() + getEquipment(Slot.BODY).getDexterity() + getEquipment(Slot.LEGS).getDexterity());
        this.totalPriamryAttribut.setIntelligence(basePrimayAttribut.getIntelligence() + getEquipment(Slot.HEAD).getIntelligence() + getEquipment(Slot.BODY).getIntelligence() + getEquipment(Slot.LEGS).getIntelligence());
        this.totalPriamryAttribut.setVitality(basePrimayAttribut.getVitality() + getEquipment(Slot.HEAD).getVitality() + getEquipment(Slot.BODY).getVitality() + getEquipment(Slot.LEGS).getVitality());

    }

    public void setSecondaryStats() {
        this.secondaryAttribut.setHealth(this.totalPriamryAttribut.getVitality());
        this.secondaryAttribut.setArmorRating(this.totalPriamryAttribut.getStrength(), this.totalPriamryAttribut.getDexterity());
        this.secondaryAttribut.setElementalResistance(this.totalPriamryAttribut.getIntelligence());
    }

    /*public void setTotalPriamryAttribut(PrimaryAttribut totalPriamryAttribut) {
        this.totalPriamryAttribut = totalPriamryAttribut;
    }*/
    public Item getWeapon() {
        return equipment.get(Slot.WEAPON);
    }

    public abstract void setHeroDPS(Item weapon);

    public abstract double getHeroDPS();


}
