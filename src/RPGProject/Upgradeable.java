package RPGProject;

import java.util.HashMap;

public interface Upgradeable {

    boolean equipArmor(Slot slot, Item item) throws RBGExceptions;
    boolean equipWeapon(Slot slot, Item item) throws RBGExceptions;
     void levelUp();
     Item getEquipment(Slot slot);

}
