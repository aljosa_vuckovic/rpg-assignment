package RPGProject;

public class SecondaryAttribut {
    private double health;
    private double armorRating;
    private double ElementalResistance;

    public SecondaryAttribut(){}

    public SecondaryAttribut( int strength, int dexterity, int intelligence, int vitality) {
        this.health = vitality*10;
        this.armorRating = strength+dexterity;
        ElementalResistance = intelligence;
    }

    public double getHealth() {
        return health;
    }

    public void setHealth(double vitality) {
        this.health = vitality*10;
    }

    public double getArmorRating() {
        return armorRating;
    }

    public void setArmorRating(double strength, double dexterity) {
        this.armorRating = strength+dexterity;
    }

    public double getElementalResistance() {
        return ElementalResistance;
    }

    public void setElementalResistance(double intelligence) {
        ElementalResistance = intelligence;
    }


}
