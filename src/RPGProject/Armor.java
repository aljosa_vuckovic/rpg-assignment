package RPGProject;

public enum Armor {

    CLOTH(1,2,5,2),
    LEATHER(2,4,1,3),
    MAIL(3,3,1,4),
    PLATE(1.0,0,0,2.0),
    NONE(0,0,0,0);

    double strength;
    double dexterity;
    double intelligence;
    double vitality;


    Armor(double strength, double dexterity, double intelligence, double vitality){
        this.strength=strength;
        this.dexterity=dexterity;
        this.intelligence=intelligence;
        this.vitality=vitality;
    }



}
