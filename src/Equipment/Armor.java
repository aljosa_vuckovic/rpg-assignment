package Equipment;

public enum Armor {

    CLOTH(1,1,5,3),
    LEATHER(1,5,1,2),
    MAIL(1,5,1,2),
    PLATE(1.0,0,0,2.0),
    NONE(0,0,0,0);

    double strength;
    double dexterity;
    double intelligence;
    double vitality;


    Armor(double strength, double dexterity, double intelligence, double vitality){
        this.strength=strength;
        this.dexterity=dexterity;
        this.intelligence=intelligence;
        this.vitality=vitality;
    }



}
