package Equipment;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON,
    NOONE;

}
