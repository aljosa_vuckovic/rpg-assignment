package Equipment;

 public enum Weapons {

        AXES(7.0, 1.1),
        BOWS(10, 4),
        DAGGERS(25, 2),
        HAMMERS(30, 1),
        STAFFS(50, 10),
        SWORDS(25, 1),
        WANDS(40, 1),
        NONE(0,0);

        double baseDamage;
        double attackPerSecond;


        Weapons(double baseDamage, double attackPerSecond) {
            this.baseDamage = baseDamage;
            this.attackPerSecond = attackPerSecond;
        }
}
