package Characters;

import Attributes.PrimaryAttribut;
import Attributes.SecondaryAttribut;
import Equipment.Item;
import Equipment.Slot;
import Equipment.Weapons;

import java.util.HashMap;
import java.util.List;

public abstract class Hero implements Upgradeable,LvlUpp {

    protected String name;
    protected double level;
    protected PrimaryAttribut basePrimayAttribut = new PrimaryAttribut();
    protected PrimaryAttribut totalPriamryAttribut = new PrimaryAttribut();
    protected SecondaryAttribut secondaryAttribut = new SecondaryAttribut();
    protected HashMap<Slot, Item> equipment = new HashMap<>();
    protected List<Weapons> useableWeapons;
    protected double heroDPS=1.0;


    public Hero() {

    }


    public Hero(String name, double level) {
        this.name = name;
        this.level = level;
    }

    public Hero(String name, double level, double strength, double vitality, double intellect, double dexterity) {
        this.name = name;
        this.level = level;
        this.basePrimayAttribut.setStrength(strength);
        this.basePrimayAttribut.setDexterity(dexterity);
        this.basePrimayAttribut.setIntelligence(intellect);
        this.basePrimayAttribut.setVitality(vitality);
        equipment.put(Slot.WEAPON, new Item());
        equipment.put(Slot.HEAD, new Item());
        equipment.put(Slot.BODY, new Item());
        equipment.put(Slot.LEGS, new Item());
        setTotalPriamryAttribut();
        setSecondaryStats();
    }

    public HashMap<Slot, Item> getAllEquipment() {
        return equipment;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLevel() {
        return level;
    }

    /** The method checks it the parameter (level) is
     * a) at least one level above the characters current level
     * b) not a negative digit or a 0
     * if a condition is met a custom IllegalArgumentException
     */
    public void setLevel(double level) {

        if (getLevel() >= level) {

            throw new IllegalArgumentException(" The hero is already level " + getLevel() + " it cannot gain level " + level);
        }else if (level<=0){
            throw new IllegalArgumentException(" The hero is already level " + getLevel() + " it cannot gain level " + level);
        }
        this.level = level;
    }

    /**
         * The parameters are used to set the (base) primary attributes of a characters. The method is mostly called upon in other methods like "levelUp" or "equip"
     * @param strength
     * @param vitality
     * @param intellect
     * @param dexterity
     */

    public void setBasePrimayAttribut(double strength, double vitality, double intellect, double dexterity) {
        basePrimayAttribut.setStrength(strength);
        basePrimayAttribut.setVitality(vitality);
        basePrimayAttribut.setIntelligence(intellect);
        basePrimayAttribut.setDexterity(dexterity);
    }


    public PrimaryAttribut getBasePrimayAttribut() {
        return basePrimayAttribut;
    }

    public PrimaryAttribut getTotalPriamryAttribut() {
        return totalPriamryAttribut;
    }

    public SecondaryAttribut getSecondaryAttribut() {
        return secondaryAttribut;
    }

    /**
     * The parameters is used to recalculate primary attributes and set them as total attributes.
     * Instead of parameter the values are called upon directly in the method
     * The method is often used in other method to reculcaute and keep the characters statistics up to date
     */


    public void setTotalPriamryAttribut() {

        this.totalPriamryAttribut.setStrength(
                basePrimayAttribut.getStrength() + getEquipment(Slot.HEAD).getStrength() +
                                                   getEquipment(Slot.BODY).getStrength() +
                                                   getEquipment(Slot.LEGS).getStrength());

        this.totalPriamryAttribut.setDexterity(
                basePrimayAttribut.getDexterity() + getEquipment(Slot.HEAD).getDexterity() +
                                                    getEquipment(Slot.BODY).getDexterity() +
                                                    getEquipment(Slot.LEGS).getDexterity());

        this.totalPriamryAttribut.setIntelligence(
                basePrimayAttribut.getIntelligence() + getEquipment(Slot.HEAD).getIntelligence() +
                                                       getEquipment(Slot.BODY).getIntelligence() +
                                                       getEquipment(Slot.LEGS).getIntelligence());

        this.totalPriamryAttribut.setVitality(
                basePrimayAttribut.getVitality() + getEquipment(Slot.HEAD).getVitality() +
                                                   getEquipment(Slot.BODY).getVitality() +
                                                   getEquipment(Slot.LEGS).getVitality());

    }

    /**
     * Similar to to the method above this method is used to reculculate secondary attributed.
     * The values are called upon inside the method
     * The method is often used in other method to reculcaute and keep the characters statistics up to date
     */
    public void setSecondaryStats() {
        this.secondaryAttribut.setHealth(this.totalPriamryAttribut.getVitality());
        this.secondaryAttribut.setArmorRating(this.totalPriamryAttribut.getStrength(), this.totalPriamryAttribut.getDexterity());
        this.secondaryAttribut.setElementalResistance(this.totalPriamryAttribut.getIntelligence());
    }


    public Item getWeapon() {
        return equipment.get(Slot.WEAPON);
    }

    public abstract void setHeroDPS(Item weapon);

    public abstract double getHeroDPS();


}
