package Characters;

import Equipment.Armor;
import Equipment.Item;
import Equipment.Slot;
import Equipment.Weapons;

public class Warrior extends Hero {

    public Warrior(String name, double level) {
        super(name, level, 5.0, 10.0, 1.0, 2.0);
    }


    /**
     *
     * @param slot Is used to decide which part of the body is about to get equiped.
     * @param item The item that is about to get equiped.
     * The method checks if the item is valid for the character by comparing the item lvl to the characters level and the item type to the character class.
     * @return Returns a boolean value depending on if the requirements are met or not.
     * @throws RPGExceptions If one of the two requirements are not met a custom exception (RPGExcpetion) is thrown.
     */
    @Override
    public boolean equipArmor(Slot slot, Item item) throws RPGExceptions {

        boolean wrongArmorType = true;
        boolean lvlToHigh = true;
        if (item.getArmor() != Armor.NONE
                && item.getArmor() == Armor.PLATE
                && item.getSlot() == slot) {
            wrongArmorType = false;
            if (this.getLevel() >= item.getLvlReg()) {
                getAllEquipment().put(slot, item);
                lvlToHigh = false;
            }

        }
        if (wrongArmorType) {
            throw new RPGExceptions("Wrong Armor type");

        } else if (!wrongArmorType) {
            if (lvlToHigh) {
                throw new RPGExceptions("Armor has to high lvl");
            }
        }


        setTotalPriamryAttribut();
        setSecondaryStats();
        if (wrongArmorType || lvlToHigh) {
            return false;
        } else {
            return true;
        }
    }
    /**
     *
     * @param slot Is used to decide which part of the body is about to get equiped.
     * @param item The item that is about to get equiped.
     * The method checks if the item is valid for the character by comparing the item lvl to the characters level and the item type to the character class.
     * @return Returns a boolean value depending on if the requirements are met or not.
     * @throws RPGExceptions If one of the two requirements are not met a custom exception (RPGExcpetion) is thrown.
     */

    @Override
    public boolean equipWeapon(Slot slot, Item item) throws RPGExceptions {
        boolean wrongWeaponType = true;
        boolean lvlToHigh = true;

        if (item.getWeapons() != Weapons.NONE
                && (item.getWeapons() == Weapons.AXES || item.getWeapons() == Weapons.HAMMERS || item.getWeapons() == Weapons.SWORDS)
                && item.getSlot() == slot) {
            wrongWeaponType = false;
            if (this.getLevel() >= item.getLvlReg()) {
                getAllEquipment().put(slot, item);
                lvlToHigh = false;
            }
        }
        if (wrongWeaponType) {
            throw new RPGExceptions("Wrong weapon type");
        } else if (!wrongWeaponType) {
            if (lvlToHigh) {
                throw new RPGExceptions("Weapon has to high lvl");
            }
        }
        setTotalPriamryAttribut();
        setSecondaryStats();

        if (wrongWeaponType || lvlToHigh) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * The method is adds upon base primary attributes with custom values for each class.
     * The method recalculates the total and secondary attributes once the base primary attributes are added upon.
     * The method increments the level of the character.
     */
    @Override
    public void levelUp() {
        setBasePrimayAttribut(
                this.basePrimayAttribut.getStrength() + 3,
                this.basePrimayAttribut.getVitality() + 5,
                this.basePrimayAttribut.getIntelligence() + 1,
                this.basePrimayAttribut.getDexterity() + 2);

        setTotalPriamryAttribut();

        setSecondaryStats();

        setLevel(getLevel() + 1.0);
    }

    @Override
    public Item getEquipment(Slot slot) {
        return getAllEquipment().get(slot);
    }

    @Override
    public void setHeroDPS(Item weapon) {
        heroDPS = (weapon.getWeaponDps() * (1.0 + (getTotalPriamryAttribut().getStrength() / 100.0)));
    }

    @Override
    public double getHeroDPS() {
        return heroDPS;
    }


}