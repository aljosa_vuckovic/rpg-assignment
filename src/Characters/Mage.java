package Characters;

import Equipment.Armor;
import Equipment.Item;
import Equipment.Slot;
import Equipment.Weapons;

public class Mage extends Hero {

    public Mage(String name, int level) {
        super(name, level, 1, 5, 8, 1);
    }


    @Override
    public boolean equipArmor(Slot slot, Item item) {

        boolean wrongArmorType = true;
        boolean lvlToHigh = true;

        try {

            if (item.getArmor() != Armor.NONE
                    && item.getArmor() == Armor.CLOTH
                    && item.getSlot() == slot) {
                wrongArmorType = false;
                if (this.getLevel() >= item.getLvlReg()) {
                    getAllEquipment().put(slot, item);
                    lvlToHigh = false;
                }

            }
            if (wrongArmorType) {
                throw new RPGExceptions("Wrong Armor type");
            } else if (!wrongArmorType) {
                if (lvlToHigh) {
                    throw new RPGExceptions("Armor has to high lvl");
                }
            }
        } catch (RPGExceptions ex) {
            System.out.println(ex.getMessage());
        }

        setTotalPriamryAttribut();
        setSecondaryStats();
        if (wrongArmorType || lvlToHigh) {
            return false;
        } else {
            return true;
        }

    }


    @Override
    public boolean equipWeapon(Slot slot, Item item) throws RPGExceptions {

        boolean wrongWeaponType = true;
        boolean lvlToHigh = true;
        if (item.getWeapons() != Weapons.NONE
                && (item.getWeapons() == Weapons.STAFFS || item.getWeapons() == Weapons.WANDS)
                && item.getSlot() == slot) {
            wrongWeaponType = false;
            if (this.getLevel() >= item.getLvlReg()) {
                getAllEquipment().put(slot, item);
                lvlToHigh = false;
            }
        }
        if (wrongWeaponType) {
            throw new RPGExceptions("Wrong weapon type");
        } else if (!wrongWeaponType) {
            if (lvlToHigh) {
                throw new RPGExceptions("Weapon has to high lvl");
            }
        }

        setTotalPriamryAttribut();
        setSecondaryStats();
        if (wrongWeaponType || lvlToHigh) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void levelUp() {
        setBasePrimayAttribut(
                this.basePrimayAttribut.getStrength() + 1,
                this.basePrimayAttribut.getVitality() + 3,
                this.basePrimayAttribut.getIntelligence() + 5,
                this.basePrimayAttribut.getDexterity() + 1);

        setTotalPriamryAttribut();

        setSecondaryStats();

        setLevel(getLevel() + 1);
    }

    @Override
    public Item getEquipment(Slot slot) {
        return getAllEquipment().get(slot);
    }

    @Override
    public void setHeroDPS(Item weapon) {
        heroDPS = weapon.getWeaponDps() * (1 + (getTotalPriamryAttribut().getStrength() / 100));
    }

    @Override
    public double getHeroDPS() {
        return heroDPS;
    }
}
