import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CountFridaysKata {

    public static void main(String[] args) throws ParseException {

        String[] days = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};


        String dateString = "13/01/2021";
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = dateFormat.parse(dateString);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int counterTwo = 0;

        for (int i = 2021; i <= 3000; i++) {


            int counter = 0;
            int months = 0;
            while (months++ < 12) {
                if (Calendar.FRIDAY == calendar.get(Calendar.DAY_OF_WEEK)) {
                    counter++;
                    counterTwo++;
                }
                calendar.add(Calendar.MONTH, 1);
            }

            System.out.println("number of Fridays on 13th = " + counter + " ");


        }
        System.out.println();
        System.out.println(counterTwo);
    }


}
